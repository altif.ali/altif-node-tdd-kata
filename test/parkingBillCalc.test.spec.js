const {calc} = require('../parkingBillCalc')

describe('Parking bill calc tests', () => {
    it('should accept a string in the format HH:MM,HH:MM and return a number', () => {
        const result = calc('10:00,11:00');
        expect(typeof result).toBe('number');
    });

    it.each([['10:55,10:57', 0], ['12:51,12:52', 0], ['13:51,13:55', 0]])(
        'should charge 0 if the duration time is less than 5mins',
        (input, expected) => {
            const result = calc(input);
            expect(result).toBe(expected);
        }
    );

    it.each([['10:00,11:00', 2], ['12:00,13:00', 2], ['15:04,16:12',2]])(
        'should charge 2 for the first hour',
        (input, expected) => {
            const result = calc(input);
            expect(result).toBe(expected);
        }
    );
});
