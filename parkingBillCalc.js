function calc(input) {
    const duration = input.split(',');
    const entryTime = duration[0];
    const exitTime = duration[1];

    const entryHours = parseInt(entryTime.split(':')[0]);
    const entryMinutes = parseInt(entryTime.split(':')[1]);

    const exitHours = parseInt(exitTime.split(':')[0]);
    const exitMinutes = parseInt(exitTime.split(':')[1]);

    const durationMinutes = exitMinutes - entryMinutes;
    const durationHours = exitHours - entryHours;

    let cost = -1

    if (durationMinutes < 5 && durationMinutes !== 0 ) {
        cost = 0;
    }

    if (durationHours <= 1 && durationHours !== 0 ){
        cost = 2

    }


    return cost;

}

module.exports = { calc }
